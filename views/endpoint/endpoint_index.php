<h2>API Test</h2>
<h4>All values are case sensitive! Test endpoint is "test" and destination "Test"! Returns OK.</h4>
<h4>http://projects.diarainfra.com/xtee/</h4>


<form action="" method="post" id="apiForm" role="form">
    <div class="form-group">
        <label for="">socialId1 (String - Social id of requester)<span class="red-text">*</span></label>
        <input type="text" class="form-control" name="" id="socialId1" placeholder="socialId1" minlength="11" maxlength="11" required>
    </div>

    <div class="form-group">
        <label for="">socialId2 (String - Social id of requestee)<span class="red-text">*</span></label>
        <input type="text" class="form-control" name="" id="socialId2" placeholder="socialId2" minlength="11" maxlength="11" required>
    </div>

    <div class="form-group">
        <label for="">organization (String - Organization making the request)<span class="red-text">*</span></label>
        <input type="text" class="form-control" name="" id="organization" placeholder="organization" minlength="2" required>
    </div>

    <div class="form-group">
        <label for="">destination (String - Destination Organization. (PPA, Tootukassa, Maksuamet, Kinnisturegister, Rahvastikuregister). Test destination is "Test"!)<span class="red-text">*</span></label>
        <input type="text" class="form-control" name="" id="destination" placeholder="destination" minlength="2" required>
    </div>

    <div class="form-group">
        <label for="">endpoint (String - Specify endpoint for the request. Test endpoint is "test"!)<span class="red-text">*</span></label>
        <input type="text" class="form-control" name="" id="endpoint" placeholder="endpoint" minlength="2" required>
    </div>

    <div class="form-group">
        <label for="">request (String - Request data)<span class="red-text">*</span></label>
        <textarea name="" class="form-control" id="request" cols="30" rows="10" minlength="2" required></textarea>
    </div>

    <button type="submit" id="submit" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Submit</button>
</form>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Response</h4>
            </div>
            <div class="modal-body">
                <code id="response">

                </code>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<script>
    $("#apiForm").validate();

    $("#submit").on("click", function(e){
        e.preventDefault();
        $.post('endpoint/send',
            {
                'socialId1': $("#socialId1").val(),
                'socialId2': $("#socialId2").val(),
                'organization': $("#organization").val(),
                'destination': $("#destination").val(),
                'endpoint': $("#endpoint").val(),
                'request': $("#request").val()
            },
            function (response) {
                $('#response').html(response);
            }
        );
    });
</script>