<div class="row">

    <h1>Logs</h1>

    <div class="table-responsive">

        <table class="table table-striped table-bordered">

            <thead>

            <tr>
                <th><?= __('Date') ?></th>
                <th><?= __('Requester') ?></th>
                <th><?= __('Requestee') ?></th>
                <th><?= __('Organization') ?></th>
                <th><?= __('Destination') ?></th>
                <th><?= __('Endpoint') ?></th>
                <th><?= __('Request') ?></th>
            </tr>

            </thead>

            <tbody>

            <?php foreach ($logs as $log): ?>
                <tr>
                    <td><?= $log['date'] ?></td>
                    <td><?= $log['socialId1'] ?></td>
                    <td><?= $log['socialId2'] ?></td>
                    <td><?= $log['organization'] ?></td>
                    <td><?= $log['destination'] ?></td>
                    <td><?= $log['endpoint'] ?></td>
                    <td>
                        <!-- Trigger the modal with a button -->
                        <button type="button" class="btn btn-info btn-lg" data-toggle="modal"
                                data-target="#request<?= $log['id'] ?>"><?= __("Open") ?></button>

                        <!-- Modal -->
                        <div id="request<?= $log['id'] ?>" class="modal fade" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Request</h4>
                                    </div>
                                    <div class="modal-body">
                                        <code>
<?= $log['request'] ?>
                                        </code>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default"
                                                data-dismiss="modal"><?= __("Close") ?></button>
                                    </div>
                                </div>

                            </div>
                        </div>




                    </td>
                </tr>
            <?php endforeach; ?>

            </tbody>

        </table>

    </div>

</div>
