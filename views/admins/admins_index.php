<div class="row">

    <h1>Destinations</h1>

    <div class="table-responsive">

        <table class="table table-striped table-bordered clickable-rows">

            <thead>

            <tr>
                <th>ID</th>
                <th><?= __('Destination name') ?></th>
                <th><?= __('Destination URL') ?></th>
            </tr>

            </thead>

            <tbody>

            <?php foreach ($destinations as $destination): ?>
                <tr data-href="admins/<?= $destination['id'] ?>">
                    <td><?= $destination['id'] ?></td>
                    <td><?= $destination['destination_name'] ?></td>
                    <td><?= $destination['destination_url'] ?>
                        <btn class="btn btn-primary pull-right"><?= __("Change") ?></btn>
                    </td>
                </tr>
            <?php endforeach; ?>

            </tbody>

        </table>

    </div>

</div>
