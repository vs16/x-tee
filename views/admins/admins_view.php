<?php
/**
 * Created by PhpStorm.
 * User: Kaspar
 * Date: 09/18/2017
 * Time: 15:41
 */
?>


<form action="" method="POST" role="form">
    <legend><?= $destination['id'] ?>. <?= $destination['destination_name'] ?></legend>

    <div class="form-group">
        <label for="">URL</label>
        <input value="<?= $destination['destination_url'] ?>" type="text" class="form-control" name="destination_url"
               id="destination_url" placeholder="Insert URL">
    </div>


    <button type="submit" class="btn btn-success"><?= __("Submit") ?></button>
</form>