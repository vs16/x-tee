# x-tee
Stories: https://www.pivotaltracker.com/n/projects/2111282

Bitbucket: https://bitbucket.org/vs16/x-tee.git

Test server: https://projects.diarainfra.com/x-tee/

Swagger: https://app.swaggerhub.com/apis/xteeinfo/xtee/1.0.0

Devs: 'Kaspar.Suvi@khk.ee',
'renee.saks@khk.ee'

Client: 'HennoTäht'

## Project setup

### Clone the repo
```sh
cd /path/to/your/webserver/root/directory
git clone https://bitbucket.org/vs16/x-tee.git
```


### Install dependencies
```sh
cd x-tee
composer install
```

### Run Mailhog to intercept all email sent from your app (necessary only for development env)
https://github.com/mailhog/MailHog#getting-started

### Create configuration file
```sh
cp config.sample.php config.php
```

The default settings should be good for development. You can optionally configure settings there when deploying to live.