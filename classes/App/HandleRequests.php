<?php

namespace App;

class HandleRequests
{
    private $response;

    function sendRequest($socialId1, $socialId2, $organization, $destination, $endpoint, $request)
    {
        $url = get_one("SELECT destination_url FROM destinations WHERE destination_name = '$destination'");

        if ($url == "") {
            stop(400, "Wrong destination.");
        }

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => preg_replace('/\s/', '', $url . $endpoint),
            CURLOPT_USERAGENT => 'cUrl request',
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => array(
                "socialId1" => $socialId1,
                "socialId2" => $socialId2,
                "organization" => $organization,
                "destination" => $destination,
                "endpoint" => $endpoint,
                "request" => $request
            )
        ));

        $this->setResponse($curl);
        curl_close($curl);

    }

    private function setResponse($c)
    {
        $curl = curl_exec($c);

        if (curl_error($c)) {
            stop(400, curl_error($c));
        }

        $this->response = $curl;
    }

    final function getResponse()
    {
        return $this->response;
    }

}