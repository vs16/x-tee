<?php

namespace App;

class Requests
{

    private $socialId1;
    private $socialId2;
    private $organization;
    private $destination;
    private $request;
    private $endpoint;

    function validateRequest($post) {
        $response = array();
        $error = false;

        if (empty($post)) {
            stop(400, "Post request is empty.");
        }

        if (empty($post['socialId1'])) {
            $msg = array('type' => 'error', 'value' => 'Requester social id is missing.');
            array_push($response, $msg);
            $error = true;
        }

        if (empty($post['socialId2'])) {
            $msg = array('type' => 'error', 'value' => 'Requestee social id is missing.');
            array_push($response, $msg);
            $error = true;
        }

        if (empty($post['organization'])) {
            $msg = array('type' => 'error', 'value' => 'Organization is missing.');
            array_push($response, $msg);
            $error = true;
        }

        if (empty($post['destination'])) {

            $msg = array('type' => 'error', 'value' => 'Destination is missing.');
            array_push($response, $msg);
            $error = true;
        }

        if (empty($post['request'])) {
            $msg = array('type' => 'error', 'value' => 'Request is missing.');
            array_push($response, $msg);
            $error = true;
        }

        if (empty($post['endpoint'])) {
            $msg = array('type' => 'error', 'value' => 'Endpoint is missing.');
            array_push($response, $msg);
            $error = true;
        }

        if ($error) {
            stop(400, $response);
        }

        self::setSocialId1($post['socialId1']);
        self::setSocialId2($post['socialId2']);
        self::setOrganization($post['organization']);
        self::setDestination($post['destination']);
        self::setRequest($post['request']);
        self::setEndpoint($_POST['endpoint']);

    }

    function logRequest()
    {
        $date = date('Y-m-d H:i:s');

        $data = array('date' => $date,
            'socialId1' => $this->socialId1,
            'socialId2' => $this->socialId2,
            'organization' => $this->organization,
            'destination' => $this->destination,
            'endpoint' => $this->endpoint,
            'request' => $this->request);

        if (insert("xtee_log", $data)) {
            //echo("data logged");
        } else {
            stop(400, "Error logging data");
        };
    }

    private function setSocialId1($id1) { $this->socialId1 = $id1; }
    final public function getSocialId1() { return $this->socialId1; }

    private function setSocialId2($id2) { $this->socialId2 = $id2; }
    final public function getSocialId2() { return $this->socialId2; }

    private function setOrganization($org) { $this->organization = $org; }
    final public function getOrganization() { return $this->organization; }

    private function setDestination($d)
    {
        $this->destination = $d;
    }

    final public function getDestination()
    {
        return $this->destination;
    }

    private function setRequest($r) { $this->request = $r; }
    final public function getRequest() { return $this->request; }

    private function setEndpoint($e)
    {
        $this->endpoint = $e;
    }

    final public function getEndpoint()
    {
        return $this->endpoint;
    }
}