<?php namespace App;

class logging extends Controller
{

    function index()
    {
        $this->logs = get_all("
        SELECT
          id,
          date,
          socialId1,
          socialId2,
          organization,
          destination,
          endpoint,
          CONVERT(request USING utf8) AS request
        FROM xtee_log
        ");
    }

}