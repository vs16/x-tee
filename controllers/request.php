<?php namespace App;

class request extends Controller
{

    public $requires_auth = false;

    function index()
    {
        $input = new Requests();
        $input->validateRequest($_POST);

        $request = new HandleRequests();

        $request->sendRequest(
            $input->getSocialId1(),
            $input->getSocialId2(),
            $input->getOrganization(),
            $input->getDestination(),
            $input->getEndpoint(),
            $input->getRequest());

        $input->logRequest();

        print_r($request->getResponse());
        exit();
    }
}