<?php namespace App;

class admins extends Controller
{

    function index()
    {
        $this->destinations = get_all("SELECT * FROM destinations");
    }

    function view()
    {
        $id = $this->getId();
        $this->destination = get_first("SELECT * FROM destinations WHERE id = '{$id}'");
    }

    function POST_view()
    {
        $id = $this->getId();
        $data = $_POST;
        update("destinations", $data, "id = '{$id}'");
        header('Location: ' . BASE_URL . 'admins/');
    }

}