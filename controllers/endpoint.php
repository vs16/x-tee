<?php namespace App;

class endpoint extends Controller
{

    function index()
    {
    }

    function AJAX_send()
    {
        $input = new Requests();
        $input->validateRequest($_POST);

        $request = new HandleRequests();

        $request->sendRequest(
            $input->getSocialId1(),
            $input->getSocialId2(),
            $input->getOrganization(),
            $input->getDestination(),
            $input->getEndpoint(),
            $input->getRequest());

        $input->logRequest();

        print_r($request->getResponse());
        exit();
    }

}